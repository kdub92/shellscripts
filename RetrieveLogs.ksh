#!/bin/ksh
# -------------------------------------------------------------------------
# Title...........: RetrieveLogs.sh
# Created.........: ven. 27 janv. 2023 18:12:05 CET
# Author..........: dp403aan
# Description.....: Retrieve logs from folders, send email with attachments
# Usage...........:
# Notes...........:
# -------------------------------------------------------------------------
CTXT=USER
REPONE=/${CTXT}/log/batch/ONE/
REPTWO=/${CTXT}/log/batch/TWO/
REPTHREE=/${CTXT}/log/batch/THREE/
TMPDIR=/tmp/GETLOGS
DATE1=$(date +%Y%m%d)
DATE2=$(date +%d-%m-%Y)

# Retrieve logs
tar cvf - $(find ${REPONE} -type f -mtime -1) | gzip > ${TMPDIR}/repone_logs_${DATE1}.tgz
tar cvf - $(find ${REPTWO} -type f -mtime -1) | gzip > ${TMPDIR}/reptwo_logs_${DATE1}.tgz
tar cvf - $(find ${REPTHREE} -type f -mtime -1) | gzip > ${TMPDIR}/repthree_logs_${DATE1}.tgz

# Mail
subject="[PROJECT_NAME] Logs ${DATE2}"
to="abc@xyz.com def@xyz.com ghi@xyz.com"

( echo "The ${DATE2} requested logs are attached to this email"
   uuencode ${TMPDIR}/repone_logs_${DATE1}.tgz repone_logs_${DATE1}.tgz
   uuencode ${TMPDIR}/reptwo_logs_${DATE1}.tgz reptwo_logs_${DATE1}.tgz
   uuencode ${TMPDIR}/repthree_logs_${DATE1}.tgz repthree_logs_${DATE1}.tgz
) | mailx -s "${subject}" "${to}"
